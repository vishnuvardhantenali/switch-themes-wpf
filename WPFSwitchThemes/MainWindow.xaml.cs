﻿using System;
using System.Windows;

namespace WPFSwitchThemes
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        
        //Themes Dictionary file locations
        private const string lightTheme = @"Themes/LightDictionary.xaml";
        private const string brownTheme = @"Themes/BrownDictionary.xaml";
        private const string classicTheme = @"Themes/ClassicThemeDictionary.xaml";
        private const string modernTheme = @"Themes/ModernThemeDictionary.xaml";
        private const string professionalTheme= @"Themes/ProfessionalThemeDictionary.xaml";
        private const string vibrantTheme = @"Themes/VibrantThemeDictionary.xaml";

        private void LightTheme(object sender, RoutedEventArgs e)
        {
            ChangeTheme(new Uri(lightTheme, UriKind.Relative));
        }

        private void BrownTheme(object sender, RoutedEventArgs e)
        {
            ChangeTheme(new Uri(brownTheme, UriKind.Relative));
        }

        private void ClassicTheme(object sender, RoutedEventArgs e)
        {
            ChangeTheme(new Uri(classicTheme, UriKind.Relative));
        }

        private void ModernTheme(object sender, RoutedEventArgs e)
        {
            ChangeTheme(new Uri(modernTheme, UriKind.Relative));
        }

        private void VibrantTheme(object sender, RoutedEventArgs e)
        {
            ChangeTheme(new Uri(vibrantTheme, UriKind.Relative));
        }

        private void ProfessionalTheme(object sender, RoutedEventArgs e)
        {
            ChangeTheme(new Uri(professionalTheme, UriKind.Relative));
        }

        private void HelpClick(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("For Themes Changes, Please check the Context Menu Items", "Theme Change Suggestion..!",
                MessageBoxButton.OK, MessageBoxImage.Information);
        }


        /// <summary>
        /// Change the Themes based on the selection.
        /// </summary>
        /// <param name="themeUri"></param>
        public static void ChangeTheme(Uri themeUri)
        {
            ResourceDictionary theme = new ResourceDictionary
            {
                Source = themeUri
            };

            Application.Current.Resources.Clear();
            Application.Current.Resources.MergedDictionaries.Add(theme);
        }
    }
}
